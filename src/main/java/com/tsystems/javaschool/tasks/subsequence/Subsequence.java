package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        int count =0; //считаем кол-во совпадений
        if((x==null)||(y==null))
            throw new IllegalArgumentException();
        else
            for (Object j : y)
                if ((count < x.size()) && (x.get(count).equals(j))) count++;
        return x.size()==count;
    }
}
