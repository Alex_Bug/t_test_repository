package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null) || inputNumbers.size() < 3 || inputNumbers.size() > 10000)
            // размер массива огранмчмл, иначе происходит переполнение памяти при сотртировке
            throw new CannotBuildPyramidException();
        int height = 2;
        int min_size = 1;
        while ((min_size + height) < inputNumbers.size())
        {
            min_size+= height;
            height++;
            if ((min_size + height) > inputNumbers.size())
            {
                throw new CannotBuildPyramidException();
            }
        }
        Collections.sort(inputNumbers);
        int schet = 0;
        int width = height * 2 - 1;
        int[][] pyramid = new int[height][width];
        for (int i = 0; i < height; i++) {
            int j = height - i - 1;
            for (int l = 0; l <= i; l++) {
                pyramid[i][j] = inputNumbers.get(schet);
                schet++;
                j+=2;
            }
        }
        return pyramid;
    }
    }
