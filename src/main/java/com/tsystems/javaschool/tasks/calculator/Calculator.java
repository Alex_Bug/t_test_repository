package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

public class Calculator {

    private enum Prio {
        HIGH(2),
        MEDIUM(1),
        LOW(0);

        private int prioValue;

        Prio(int prio) {
            this.prioValue = prio;
        }

        int getPrioValue() {
            return this.prioValue;
        }
    }

    private static Map<Character, Prio> operationPrioMap = new HashMap<>();
    static {
        operationPrioMap.put('*', Prio.HIGH);
        operationPrioMap.put('/', Prio.HIGH);
        operationPrioMap.put('-', Prio.MEDIUM);
        operationPrioMap.put('+', Prio.MEDIUM);
        operationPrioMap.put('(', Prio.LOW);
        operationPrioMap.put(')', Prio.LOW);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result;
        try {
            validateInput(statement);
            String rpn = convertToRPN(statement);
            result = calculateRpn(rpn);
        } catch (ArithmeticException ae) {
            System.out.println(String.format("Expression \'%s\' cannot be evaluated.", statement));
            result = null;
        }


        return result;
    }

    //Convert to Reverse Polish notation
    private String convertToRPN(String input) {
        //(1+38)*4-5
        StringBuilder sb = new StringBuilder();
        Stack<Character> operationsStack = new Stack<Character>();
        Integer numberStartFrom = null; //For parsing numbers which more than one symbol
        for (int i = 0; i < input.length(); i++) {
            Character currentEl = input.charAt(i);
            if (currentEl == '(') {
                operationsStack.push(currentEl);
            } else if (currentEl == ')') {
                while (operationsStack.isEmpty() || operationsStack.peek() != '(') {
                    Character operationFromStack = !operationsStack.isEmpty() ? operationsStack.pop() : null;
                    if (operationFromStack != null && operationFromStack != '(') {
                        sb.append(operationFromStack);
                        sb.append(' ');
                    }
                }
                operationsStack.pop();
            } else if (operationPrioMap.keySet().contains(currentEl)) {
                Prio stackOperationPrio = !operationsStack.isEmpty() ? operationPrioMap.get(operationsStack.peek()) : null;
                Prio inputOperationPrio = operationPrioMap.get(currentEl);
                if (stackOperationPrio != null && stackOperationPrio.getPrioValue() >= inputOperationPrio.getPrioValue()) {
                    sb.append(operationsStack.pop());
                    sb.append(' ');
                }
                operationsStack.push(currentEl);
            } else if (currentEl >= '0' && currentEl <= '9' || currentEl == '.') {
                Character nextEl = i != input.length() - 1 ? input.charAt(i + 1) : null;
                if (numberStartFrom != null && (nextEl == null || (nextEl < '0' || nextEl > '9') && nextEl != '.')) {
                    sb.append(input.substring(numberStartFrom, i+1));
                    sb.append(' ');
                    numberStartFrom = null;
                    continue;
                }
                if (nextEl != null && numberStartFrom == null && (nextEl >= '0' && nextEl <= '9' || nextEl == '.')) {
                    numberStartFrom = i;
                    continue;
                }
                if (numberStartFrom != null) {
                    continue;
                }
                sb.append(currentEl);
                sb.append(' ');
            }
        }
        while (!operationsStack.isEmpty()) {
            sb.append(operationsStack.pop());
            sb.append(' ');
        }
        return sb.toString();
    }

    private String calculateRpn(String rpn) {
        String[] numbersAndOps = rpn.trim().split(" ");
        Stack<Double> numberStack = new Stack<Double>();
        for (String numberOrOperation : numbersAndOps) {
            if (Pattern.matches("[0-9,\\.]+", numberOrOperation)) {
                Double number = Double.valueOf(numberOrOperation);
                numberStack.push(number);
            } else {
                Double number2 = numberStack.pop();
                Double number1 = numberStack.pop();
                if (numberOrOperation.equals("+")) {
                    numberStack.push(number1 + number2);
                } else if (numberOrOperation.equals("-")) {
                    numberStack.push(number1 - number2);
                } else if (numberOrOperation.equals("*")) {
                    numberStack.push(number1 * number2);
                } else if (numberOrOperation.equals("/")) {
                    numberStack.push(number1 / number2);
                }
            }
        }
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        Double doubleResult = !numberStack.isEmpty() ? numberStack.pop() : null;
        if (doubleResult != null && (doubleResult.isInfinite() || doubleResult.isNaN())) {
            doubleResult = null;
        }
        return doubleResult != null ? df.format(doubleResult) : null;
    }

    private void validateInput(String input) throws ArithmeticException {
        int countOfOpeningParenthesis = countSymbols(input, "(");
        int countOfClosingParenthesis = countSymbols(input, ")");
        if (input == null || !Pattern.matches("[0-9*/+\\-().]+", input) || !Pattern.matches("^[0-9(].*", input)
                || input.contains("..") || input.contains("--") || input.contains("++")
                || input.contains("**") || input.contains("//")
                || countOfOpeningParenthesis != countOfClosingParenthesis || checkOpenParenthesAfterNumber(input)) {
            throw new ArithmeticException();
        }
    }

    private int countSymbols(String input, String c) {
        return input != null ? input.length() - input.replace(c, "").length() : 0;
    }

    private boolean checkOpenParenthesAfterNumber(String input) {
        for (int i = 1; i < input.length(); i++) {
            if (input.charAt(i) == '(' && input.charAt(i-1) >= '0' && input.charAt(i-1) <= '9') {
                return true;
            }
        }
        return false;
    }

}
